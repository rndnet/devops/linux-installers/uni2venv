#!/bin/bash
. common_params.sh
if [ ! -f "common_params.sh" ];  then
        echo "File common_params.sh not found!" ;  exit 1 ;
fi

clear

eval ${conda_init_script}
conda env list

echo

read -p "Remove by name [n] or by path [p]? " answer
if [[ $answer = n ]] ; then
    read -e -p  "Enter conda env NAME for remove: " env_cur
    conda remove --name ${env_cur} --all
elif [[ $answer = p ]] ; then
    read -e -p  "Enter conda env PATH for remove: " env_cur
    conda remove -p ${env_cur} --all
else
    echo "Unknown command: "${env_cur}
fi

if [ "${conda_init_script}" != "" ]; then
   conda deactivate # deactivate conda base
fi

echo "Current python: '"$(which python)"'"




