#!/bin/bash

if [ ! -f "common_params.sh" ];  then
    echo "File common_params.sh not found!" ;  exit 1 ;
fi
. common_params.sh


if [ -z "$strategy" ]; then
    echo -e "\n${RED} Error! 'strategy' variable not set in  'common_params.sh'. Skip. ${NC}"
    exit 1
fi

if [ "$strategy" = "src" ] ; then
	cd $src
elif [ "$strategy" = "pkg" ] ; then
    echo "Check not realized yet."
    exit 1
else
    echo "Unknown strategy: $strategy"
    exit 
fi



# Colorscheme
RESET='\e[m'
COLOR_DIR='\e[34m'
COLOR_GIT_CLEAN='\e[32m'
COLOR_GIT_DIRTY='\e[31m'

SYMBOL_GIT_MODIFIED='*'
SYMBOL_GIT_PUSH='+'
SYMBOL_GIT_PULL='-'

__git_info( ) {
    hash git 2>/dev/null || return # git not found
    local git_eng="env LANG=C git --git-dir $1/.git --work-tree $1"   # force git output in English to make our work easier

    # get current branch name
    local ref=$($git_eng symbolic-ref --short HEAD 2>/dev/null)

    if [[ -n "$ref" ]]; then
        # prepend branch symbol
        ref=$ref
    else
        # get tag name or short unique hash
        ref=$($git_eng describe --tags --always 2>/dev/null)
    fi

    [[ -n "$ref" ]] || return  # not a git repo

    local marks

    # scan first two lines of output from `git status`
    while IFS= read -r line; do
        if [[ $line =~ ^## ]]; then # header line
            [[ $line =~ ahead\ ([0-9]+) ]] && marks+=" $SYMBOL_GIT_PUSH${BASH_REMATCH[1]}"
            [[ $line =~ behind\ ([0-9]+) ]] && marks+=" $SYMBOL_GIT_PULL${BASH_REMATCH[1]}"
            echo -e -n $COLOR_GIT_CLEAN
        else # branch is modified if output contains more lines after the header line
            marks="$SYMBOL_GIT_MODIFIED$marks"
            echo -e -n $COLOR_GIT_DIRTY
            break
        fi
    done < <($git_eng status --porcelain --branch 2>/dev/null)  # note the space between the two <

    # print the git branch segment without a trailing newline
    echo -e "$ref$marks$RESET"
}

echo "Fetching..."
find . -mindepth 2 -maxdepth 2 -name .git | xargs -n1 -P8 -I{} git --git-dir {} fetch
echo "-----------"
for f in $(find . -mindepth 2 -maxdepth 2 -name .git | xargs dirname | sort); do
    printf '%-16s %s\n' "$(basename $f)" "$(__git_info ${f})"
done

