function show_installed() {
        local  modules=("${!1}")
        echo "${modules[@]}"

        for f in "${modules[@]}"
        do
         if [ -d "$f/conda-meta" ]; then
           echo
           echo "Conda env:"
           echo " "$f
           conda activate $f
           if [ $? = 0 ]; then 
             conda list
             conda deactivate
           fi
         fi
        done
}

. common_params.sh
if [ ! -f "common_params.sh" ];  then
    echo "File common_params.sh not found!" ;  exit 1 ;
fi

eval ${conda_init_script}

show_installed venv_snode[@]
show_installed venv_client[@]
show_installed venv_developer[@]
show_installed venv_smaster[@]

if [ "${conda_init_script}" != "" ]; then
       conda deactivate # deactivate conda base
fi

echo "Current python: '"$(which python)"'"
