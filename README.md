Install rndnet platform from sources or packages and install to conda environment.
---------------------------------------------------------------------------------

Semi-automated installation with default values
************************************************

1. cd libs; bash install.sh
2. Steps 11-14 in manual install.

Manual installation
*******************

1. Install system requirements
  - conda:    https://conda.io/projects/conda/en/latest/user-guide/install/index.html 
  - python3:  sudo yum install python3 (or after conda installation it is posible install python to conda base env: conda install python=3.6)
  - graphviz: sudo yum install graphviz (need for client and rndnet-server)
  - gcc-c++:  sudo yum install gcc-c++  (need for rndnet-server for ufal.udpipe python module).  
        
2. Create `common_params.sh` file
  - `cp libs/common_params_skel.sh common_params.sh`
   
3. Edit `common_params.sh`

  - By default `pkg` update strategy (install from packages) is enabled. You can change `strategy`  to `src` (install from source) if need.
  - By default `packages` variable is already set, but you can change it if need. If `src` strategy is enabled then uncomment and set valid `src` variable.
   
  - Set your clouds names (as they will register in rndnet_console) in `clouds` array. <br/>
       If your have only one cloud then `clouds=( your_cloud_name )`. <br/>
       If your have multi clouds then  `clouds=( your_cloud_name1 your_cloud_name2 )`.<br/>
   
  - Set valid `venv_src`, `smaster_servers_services` and `smaster_schedulers_services` variables.

  - Set valid `rndnet_python_version` variable.

  - Set valid `conda_init_script` variable.

4. Install
  - `./run-update-src.sh`
  - `./run-update-clients.sh`
  - `./run-update-smaster-no-restart.sh`
  - `./run-update-snodes.sh`
  - `./run-update-developers.sh`


5. Install RabbitMQ server

  -  `bash libs/rabbimq-installer.sh `
  -  `./rabbitmq-install.sh`

     Description of `rabbitmq-install.sh`: `Установка и настройка RabbitMQ` (https://docs.google.com/document/d/1Xr59uoG0GmuoH-MOkSLAVlAcq5zyYDJRNwtp0jYdB6Q/edit#heading=h.de98sojglxew)

6. Install minio object storage server

  -  `bash libs/minio-manager-installer.sh `
  -  `./minio_manager.sh`

     Description of `minio_manage.sh`: `Использование MIN.IO->Автоматизированное создание и управление` (https://docs.google.com/document/d/1Xr59uoG0GmuoH-MOkSLAVlAcq5zyYDJRNwtp0jYdB6Q/edit#heading=h.mevx0k2xc6jw)

7. Database initialize: `Сервер->Настройка БД` (https://docs.google.com/document/d/1Xr59uoG0GmuoH-MOkSLAVlAcq5zyYDJRNwtp0jYdB6Q/edit#heading=h.wr8reiyw5ukb)

  - `bash libs/rndnet-init-db-installer.sh`
  - `./rndnet-init-db.sh`

8. Cloud initialize: `Сервер->Инициализация облака->Автоматизированная` (https://docs.google.com/document/d/1Xr59uoG0GmuoH-MOkSLAVlAcq5zyYDJRNwtp0jYdB6Q/edit#heading=h.zb1qga5k504y)
  - `./run-rndnet_console.sh cloud init --host=localhost --port=5432 cloud1`
    
9. Server run

 9.1. Test start (optional) 

  - `cp libs/server_example.conf server.conf`
  - `cp libs/start-server.sh start-server.sh`
  - Set valid params in `server.conf` and add record to `.pgpass` file: `Сервер->Настройка и запуск` (https://docs.google.com/document/d/1Xr59uoG0GmuoH-MOkSLAVlAcq5zyYDJRNwtp0jYdB6Q)   
  - `./start-server.sh` <br/>
  or 
  - `mv server_config_file.json server.conf` # `server_config_file` was generated after cloud initialize.
  - `cp libs/start-server.sh start-server.sh`
  - add record to `.pgpass` file: `Сервер->Настройка и запуск сервера` (https://docs.google.com/document/d/1Xr59uoG0GmuoH-MOkSLAVlAcq5zyYDJRNwtp0jYdB6Q/edit#heading=h.a7ftvq4ob2nh)
  - `./start-server.sh`

 9.2. Install server as service: `Сервер->Создание сервиса->Автоматическое` (https://docs.google.com/document/d/1Xr59uoG0GmuoH-MOkSLAVlAcq5zyYDJRNwtp0jYdB6Q/edit#heading=h.1z7rghpatmps)
  - `cd $venv_smaster/bin/`  # $venv_smaster path from common_params.sh not real variable
  - `./rndnet_server_manager.sh` 

10. Install scheduler as service: `Облачный шедулер->Создание сервиса->Автоматическое` (https://docs.google.com/document/d/1Xr59uoG0GmuoH-MOkSLAVlAcq5zyYDJRNwtp0jYdB6Q/edit#heading=h.cyqueh8l4yyd)
  - `cd $venv_smaster/bin`  # $venv_smaster path from common_params.sh not real variable
  - `./rndnet_scheduler_manager.sh` 
   
   You can set default scheduler header as:` . ~/miniconda3/etc/profile.d/conda.sh ; conda activate ~/rndnet/conda-env/snode-modules    `
   Or if you want use different environments you can set this commands to scheduler header on project designer.


11. Client run:
  - `./run-client.sh` if run from console
  - `$venv_client/bin/cloudview` when create shortcut (and set working directory to `$venv_client/bin`. # $venv_client path from common_params.sh not real variable

12. Production use: `Сервер->Общий процесс установки` (https://docs.google.com/document/d/1Xr59uoG0GmuoH-MOkSLAVlAcq5zyYDJRNwtp0jYdB6Q/edit#heading=h.jqjbt4be9pps) 

13. Copy useful scripts (optional).

  - `cp libs/save-configs-arch.sh save-configs-arch.sh` and edit it if need.
  - `cp libs/show-status.sh show-status.sh` and edit it if need.

14. Set valid params in `run-module-julia-1.0.3.sh` (optional).
15. If you want call environments by name not only by path
  - `conda config --append envs_dirs $venv_src `


Scripts/Files description
--------------------------
  - run-rndnet_console.sh - run admin console.
  
  - update-all.sh - full update: packets or sources, database schema, all conda environments (`CE`).
  
  - run-update-src.sh - update sources (`strategy=src`) or download last packets (`strategy=pkg`)
  
  - common_params.sh - main config
  
  - check-installed.sh - list all modules in all CE
  - check-installed_rndnet.sh - list all rndnet modules in all CE
  
  Update CE 
  *******************************************************************
  Run only after run-download-modules.sh or run-update-src.sh

  - run-update-clients.sh - install/update clients CE
  - run-update-developers.sh - install/update developers CE
  - run-update-smaster.sh - install/update server/scheduler CE and restart services.
  - run-update-smaster-no-restart.sh - install/update server/scheduler CE.
  - run-update-snodes.sh - install/update compute CE
  - run-update-database.sh - update database schema (use only after server/scheduler VPE were updated).)
  
  CE administration
  *****************

  - run-command-in-env.sh - run any console command in choosen CE
  - run-update-one-env.sh - create/update choosen CE 
  - run-remove-env.sh - remove choosen CE
  
  Application administration
  **************************

  - run-client.sh - run client application
  - start-server.sh - server manual run
  - start-rndnet-services.sh - start rndnet services if exist
  - stop-rndnet-services.sh - stop rndnet services if exist
  - add_user.sh - add new user
  
  - save-configs-arch.sh - make/update archive
  - run-module-julia-1.0.3.sh - run julia (not installed by default) 


