#!/bin/bash
. common_params.sh
if [ ! -f "common_params.sh" ];  then
     echo "File common_params.sh not found!" ;  exit 1 ;
fi

eval ${conda_init_script}

echo "Client conda env"
conda activate $venv_client
if [ $? = 0 ]; then
   cloudview 
   conda deactivate
fi

if [ "${conda_init_script}" != "" ]; then
  conda deactivate # deactivate conda base
fi


echo "Current python: '"$(which python)"'"


