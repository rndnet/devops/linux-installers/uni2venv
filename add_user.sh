#!/bin/bash

. common_params.sh
if [ ! -f "common_params.sh" ];  then
   echo "File common_params.sh not found!" ;  exit 1 ;
fi

echo ${smaster_services[*]}


echo Known clouds: ${clouds[*]}
echo
read -p "Input cloud name: " cloud_name


#for i in "${clouds[@]}"
#do
#   echo i
#done

eval ${conda_init_script}

echo "Smaster conda  env"
conda activate $venv_smaster
if [ $? = 0 ]; then
  read -p "Enter new user name for '"${cloud_name}" 'cloud: " user_name
  rndnet_console user add $user_name --cloud=${cloud_name}
  conda deactivate
fi


if [ "${conda_init_script}" != "" ]; then
   conda deactivate # deactivate conda base
fi
       
echo "Current python: '"$(which python)"'"




