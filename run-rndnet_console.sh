#!/bin/bash
ABSOLUTE_FILENAME=`readlink -e "$0"`
FILE_DIRECTORY=`dirname "$ABSOLUTE_FILENAME"` # Define script directory
if [ ! -f "$FILE_DIRECTORY/common_params.sh" ];  then
         echo "$FILE_DIRECTORY/common_params.sh not found!" ;  exit 1 ;
     fi

. $FILE_DIRECTORY/common_params.sh

eval ${conda_init_script}

echo "Smaster conda env"
conda activate $venv_smaster
if [ $? = 0 ]; then
  rndnet_console "$@"
  conda deactivate
fi

if [ "${conda_init_script}" != "" ]; then
   conda deactivate # deactivate conda base
fi

echo "Current python: '"$(which python)"'"




