#!/bin/bash
#conda install -c conda-forge julia=1.0.3
#conda env config vars set JULIA_DEPOT_PATH=/nfs0/r003/rndnet/julia-home/julia-1.0.3
#conda env config vars list
#rndnet bfj install: julia -E 'import Pkg;Pkg.add(Pkg.PackageSpec(url="https://gitlab.com/rndnet/apps/BFJ.jl",rev="master"))'

clear

. common_params.sh
if [ ! -f "common_params.sh" ];  then
     echo "File common_params.sh not found!" ;  exit 1 ;
fi

eval ${conda_init_script}

conda activate ${venv_client}

#export JULIA_DEPOT_PATH=/nfs0/r003/rndnet/julia-home/julia-1.0.3  # do not need if set in conda env

echo "DEPOT_PATH =" $(julia -E 'DEPOT_PATH')
echo
echo "Pkg.installed =" $(julia -E ' import Pkg;Pkg.installed()')
echo

julia

conda deactivate

if [ "${conda_init_script}" != "" ]; then
   conda deactivate # deactivate conda base
fi

echo "Current python: '"$(which python)"'"


