#!/bin/bash
cd libs
. common.sh
if [ ! -f "common.sh" ];  then
         echo "File common.sh not found!" ;  exit 1 ;
fi

if [ -z "$smaster_servers_services" ]; then
     echo -e "\n${RED}Servers services not set. Skip.${NC}\n"
else
     echo -e "\n${RED}Starting servers...${NC}\n"
     start_array smaster_servers_services[@]
     sleep 10
fi

if [ -z "$smaster_schedulers_services" ]; then
     echo -e "\n${RED}Schedulers services not set. Skip.${NC}\n"
else
     echo -e "\n${RED}Starting schedulers...${NC}\n"
     start_array smaster_schedulers_services[@]
fi
