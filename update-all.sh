#!/bin/bash

pause_len=5

clear
if [ ! -f "common_params.sh" ];  then
    echo "File common_params.sh not found!" ;  exit 1 ;
fi
. common_params.sh

curdir=$(pwd)

./check-status.sh

read -t ${pause_len} -n 1 -s -r -p "Press any key to continue for update"
echo
cd $curdir
./run-update-src.sh
echo "Sources/packages Updated"
echo

read -t ${pause_len} -n 1 -s -r -p "Press any key to continue for client update"
echo
cd $curdir
./run-update-clients.sh
echo "Clients updated"
echo

read -t ${pause_len} -n 1 -s -r -p "Press any key to continue for snode update"
cd $curdir
./run-update-snodes.sh
echo "Snode updated"
echo

read -t ${pause_len} -n 1 -s -r -p "Press any key to continue for developers update"
echo
cd $curdir
./run-update-developers.sh
echo "Developers updated"
echo

read -t ${pause_len} -n 1 -s -r -p "Press any key to continue for server update"
echo
cd $curdir
./run-update-smaster-no-restart.sh
echo "Server updated"
echo

read -t ${pause_len} -n 1 -s -r -p "Press any key to continue for database update"
echo
cd $curdir
bash -euo pipefail run-update-database-and-log.sh
if [[ $? == 1 ]]; then
   read -n 1 -s -r -p "Database update error!"
   exit 1
fi

echo "Database updated"

read -t ${pause_len} -n 1 -s -r -p "Press any key to continue for services restart"
echo
cd $curdir
bash libs/update.sh smaster-restart 2>&1 | tee -a logs/logging-smaster-update.log
echo "Services restarted"

