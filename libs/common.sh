ABSOLUTE_FILENAME=`readlink -e "$0"`
FILE_DIRECTORY=`dirname "$ABSOLUTE_FILENAME"` # Define script directory
if [ ! -f "$FILE_DIRECTORY/../common_params.sh" ];  then
     echo "$FILE_DIRECTORY/../common_params.sh not found!" ;  exit 1 ;
fi
. $FILE_DIRECTORY/../common_params.sh

CUR_USER=$USER
RED='\033[0;31m'
NC='\033[0m' # No Color

echo ---------------------------------------------
echo $(date)
echo 

if [ $CUR_USER = 'root' ]
then
    echo Do not use root user here!
    exit
fi

if [ -z "$strategy" ]; then
    echo -e "\n${RED} Error! 'strategy' variable not set in 'common_params.sh'. ${NC}"
    exit 1
fi

if [ "$strategy" = "src" ] ; then
    echo Sources dir: $src
    SCHEME_FILE=$src/server/rndnet_server/console/schema.py
elif [ "$strategy" = "pkg" ] ; then
    echo Packages dir: $packages
    echo RnDnet repository: ${rndnet_rep}

    SCHEME_FILES=$(find $venv_smaster/lib/ -name schema.py)
    echo Database scheme files found: $SCHEME_FILES

    SCHEME_FILE=$(find $venv_smaster/lib/ -name schema.py -print -quit)
else 
    echo -e "\n${RED} Unknown strategy: $strategy  ${NC}"
    exit 1
fi

echo Smaster servers services: ${smaster_servers_services[*]}
echo Smaster update servers services: ${smaster_update_servers_services[*]}
echo Smaster schedulers services: ${smaster_schedulers_services[*]}
echo Client bundle: ${client_bundle[*]}
echo Compute node bundle: ${compute_node_bundle[*]}
echo Developer bundle: ${develop_bundle[*]}
echo Smaster bundle: ${smaster_bundle[*]}
echo Super bundle: ${super_bundle[*]}
echo Clouds: ${clouds[*]}
echo
echo Snode env: ${venv_snode[*]}
echo Client env: ${venv_client[*]}
echo Developer env: ${venv_developer[*]}
echo Smaster env: ${venv_smaster}

echo Conda init script: ${conda_init_script}
echo Database scheme: ${SCHEME_FILE}
echo --------------------------------------------

CUR_DIR=$(pwd)

function install_array() {
    local  modules=("${!1}")
    #echo "${modules[@]}"
    local env_dir="${2}"
    
    if [[ ! -n ${env_dir} ]]; then
        echo -e "${RED}The path to folder for conda environment is empty! Exit!${NC}"
        exit 1
    fi
    
    #if [[ ! -n ${conda_init_script} ]]; then
    #    echo -e "${RED}The 'conda_init_script' variablet is empty! Exit!${NC}"
    #    exit 1
    #fi
        
    eval ${conda_init_script}
    
    if [ ! -d ${env_dir} ]; then
        echo -e "\nConda env not exist yet: '"${env_dir}"'"

       read -e -p 'Enter python version: ' -i ${rndnet_python_version} python_version
       #read -t 30 -n 1 -s -r -e -p 'Enter python version: ' -i ${rndnet_python_version} python_version
       #echo

       conda create  -p ${env_dir} python=${python_version}

       echo -e "\n${RED} Conda env created in '${env_dir}'${NC}"
    else 
       echo -e "\nConda env already exist: '"${env_dir}"'"
    fi
    
    #local env_name=$(basename ${env_dir})
    #conda env list | grep ${env_name}

    conda activate ${env_dir}
    echo -e "\n${RED} Conda env activated in '${env_dir}'${NC}"

    echo -e "\nVirtual python path: "$(which python)

    for i in "${modules[@]}"
    do
        echo -e "\n${RED} Install rndnet/$i in '${env_dir}'  ${NC}"

        if [ "$strategy" = "src" ] ; then
	    pip install -r ${src}/${i}/requirements.txt
            pip install -U ${src}/${i}
        elif [ "$strategy" = "pkg" ] ; then
            pip install -r ${packages}/${i}.requirements.txt
            pip install -U --upgrade-strategy eager  ${i} --no-index --find-links ${packages}
        else 
            echo -e "\n${RED} Unknown strategy: $strategy  ${NC}"
        fi

    done

    conda deactivate # deactivate current conda env	
    echo -e "\nConda env deactivated from  ${env_dir}. Python path: "$(which python)
    if [ "${conda_init_script}" != "" ]; then
       conda deactivate # deactivate conda base
       echo -e "\nConda base deactivated. Python path: "$(which python)
    fi
}

function update_array() {
   if [ "$strategy" = "src" ] ; then
        local  modules=("${!1}")
        #echo "${modules[@]}"

        for i in "${modules[@]}"
        do
            if [ ! -f "${src}/${i}/.git/config" ]; then
                echo -e "\n${RED} Clone rndnet/$i sources ${NC}"
                git clone ${git_src}/${i}.git ${src}/${i}

            else
                echo -e "\n${RED} Update rndnet/$i sources ${NC}"
                git --git-dir=$src/$i/.git --work-tree=$src/$i/ pull
            fi
                
        done
   elif [ "$strategy" = "pkg" ] ; then

            if [ -z "$packages" ]; then
                echo " Warining! 'packages' variable not set in'common_params.sh'. Skip"
                exit
            fi

            mkdir -p $packages
    	    
            eval ${conda_init_script}

            wget -O $packages/bfj.requirements.txt                $rndnet_rep/bfj/requirements.txt
            wget -O $packages/rndnet-scheduler.requirements.txt   $rndnet_rep/rndnet-scheduler/requirements.txt
            wget -O $packages/rndnet-client.requirements.txt      $rndnet_rep/rndnet-client/requirements.txt
            wget -O $packages/rndnet-server.requirements.txt      $rndnet_rep/rndnet-server/requirements.txt

            pip download -d $packages/ --index-url $rndnet_rep bfj
            pip download -d $packages/ --index-url $rndnet_rep rndnet-scheduler
            pip download -d $packages/ --index-url $rndnet_rep rndnet-client
            pip download -d $packages/ --index-url $rndnet_rep rndnet-server

   else
        echo -e "\n${RED} Can not update for this strategy: $strategy  ${NC}"
   fi
}

function stop_array() {

    local  modules=("${!1}")
    #echo "${modules[@]}"

    for i in "${modules[@]}"
    do
        echo -e "${RED} Stop $i service ${NC}"
        sudo systemctl stop $i
    done
}

function start_array() {
    local  modules=("${!1}")
    #echo "${modules[@]}"

    for i in "${modules[@]}"
    do
        echo -e "${RED} Start $i service ${NC}"
        sudo systemctl start $i
        systemctl status $i
    done
}

function client_dialog() {
        local text="$1"
        read -p "$1: " UPDATE_CLIENT

        if [ -n "$UPDATE_CLIENT" ]
        then
            echo -e  "${RED}Install current version of rndnet to ${NC}"$UPDATE_CLIENT
        else 
            echo Node name is empty!
            exit
        fi
}
