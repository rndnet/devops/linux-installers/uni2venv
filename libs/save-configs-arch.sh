#!/bin/bash
arch=$(pwd)/../configs

. common_params.sh

minios=( minio )  # Minio services names

cp -vu common_params.sh $arch/common_params.sh
cp -vu save-configs-arch.sh  $arch/save-configs-arch.sh

# Save minio files
mkdir -p $arch/etc/default
mkdir -p $arch/etc/systemd/system
for i in "${minios[@]}"
do
    cp -vu /etc/default/${i}  $arch/etc/default/${i}
    #cp -vu /etc/systemd/system/${i}.service $arch/etc/systemd/system/${i}.service
    cp -vu /etc/systemd/system/${i%.*}.service $arch/etc/systemd/system/${i%.*}.service # remove extension
done

#Save rndnet-servers files
mkdir -p $arch/$HOME/.config/blockflow
cp -vu $HOME/.config/blockflow/rndnet_console.json $arch/$HOME/.config/blockflow/rndnet_console.json

if [ -z "$smaster_servers_services" ]; then
   echo "The 'smaster_servers_services' variable not set in 'common_params.sh'. Skip."
else
  mkdir -p $arch/etc/rndnet_server

  for i in "${smaster_servers_services[@]}"
  do
      cp -vu /etc/rndnet_server/${i}.conf $arch/etc/rndnet_server/${i}.conf
      cp -vu /etc/systemd/system/${i}.service $arch/etc/systemd/system/${i}.service
  done
fi

#Save rndnet-schedulers fiiles
if [ -z "${smaster_schedulers_services}" ]; then
   echo "The 'smaster_schedulers_services' variable not set in 'common_params.sh'. Skip."
else
  mkdir -p $arch/etc/rndnet_scheduler

  for i in "${smaster_schedulers_services[@]}"
  do
      cp -vu /etc/rndnet_scheduler/${i}.json $arch/etc/rndnet_scheduler/${i}.json
      cp -vu /etc/systemd/system/${i}.service $arch/etc/systemd/system/${i}.service
  done
fi 

#Save nginx files
mkdir -p $arch/etc/nginx
cp -vu /etc/nginx/nginx.conf $arch/etc/nginx/nginx.conf
mkdir -p $arch/etc/nginx/sites-enabled
cp -vu /etc/nginx/sites-enabled/* $arch/etc/nginx/sites-enabled/

