strategy=pkg                      # src - use source for updates ; pkg - use packages for updates

#src=/opt/rndnet/src              # Clone sources to this directory (only if use src strategy )
packages=~/rndnet/packages        # Download packages to this directory (only if use pkg  strategy )

venv_src=~/rndnet/conda-env       # Create conda environments in this directory
clouds=( cloud1  )                # Clouds registered names

smaster_servers_services=( rndnet_server_${clouds[0]}  ) # rndnet servers services names
smaster_schedulers_services=( rndnet_scheduler_${clouds[0]} )    # rndnet schedulers services names

#smaster_update_servers_services=( rndnet_server_${clouds[0]}_backup  ) # update rndnet servers services names. this server start before main servers stopped when they must be update
#smaster_pre_update="sudo cp -v /etc/nginx/nginx.conf_update  /etc/nginx/nginx.conf && sudo systemctl reload nginx && cat /etc/nginx/nginx.conf | grep Active"  # Run command before update servers start
#smaster_post_update="sudo cp -v /etc/nginx/nginx.conf_main  /etc/nginx/nginx.conf && sudo systemctl reload nginx && cat /etc/nginx/nginx.conf | grep Active" # Run command before update servers stop

rndnet_python_version=3.6.9
conda_init_script=". /usr/etc/profile.d/conda.sh" # 
#conda_init_script=". ~/miniconda3/etc/profile.d/conda.sh"

venv_snode=( $venv_src/snode-modules )
venv_client=( $venv_src/client-modules )
venv_developer=( $venv_src/developer-modules )
venv_smaster=$venv_src/smaster-modules

rndnet_rep=https://server1.rndnet.net/static/simple
git_src=git@gitlab.com:rndnet/apps     # Application main repository: git protocol
#git_src=https://gitlab.com/rndnet/apps  # Application main repository: https protocol

if [ "$strategy" = "src" ] ; then
	client_bundle=( common bfj.py scheduler client )
	develop_bundle=( server bfj.py common scheduler client )
	compute_node_bundle=( bfj.py )
	smaster_bundle=( server common scheduler )
	super_bundle=( server bfj.py common scheduler client )
elif [ "$strategy" = "pkg" ] ; then
	client_bundle=( rndnet-client )
	develop_bundle=( rndnet-server rndnet-client )
	compute_node_bundle=( bfj )
	smaster_bundle=( rndnet-server rndnet-scheduler )
	super_bundle=( rndnet-server rndnet-client )
else
    if [ -z "$strategy" ]; then
       echo " Error! 'strategy' variable not set in 'common_params.sh'"
    else
       echo "Unknown strategy: $strategy"
    fi

    exit 1
fi
