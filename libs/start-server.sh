#!/bin/bash
. common_params.sh
if [ ! -f "common_params.sh" ];  then
        echo "File common_params.sh not found!" ;  exit 1 ;
fi

eval ${conda_init_script}

echo "Smaster conda env"
conda activate $venv_smaster
if [ $? = 0 ]; then
  rndnet_server --config server.conf --accesslog -  --bind 0.0.0.0:5000
  conda deactivate
fi

if [ "${conda_init_script}" != "" ]; then
   conda deactivate # deactivate conda base
fi

echo "Current python: '"$(which python)"'"




