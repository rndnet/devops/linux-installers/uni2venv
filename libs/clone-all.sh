
echo "Use run-update-src.sh!"
exit

ABSOLUTE_FILENAME=`readlink -e "$0"`
FILE_DIRECTORY=`dirname "$ABSOLUTE_FILENAME"` # Define script directory
if [ ! -f "$FILE_DIRECTORY/../common_params.sh" ];  then
    echo "$FILE_DIRECTORY/../common_params.sh not found!" ;  exit 1 ;
fi
. $FILE_DIRECTORY/../common_params.sh

cd $src

git clone git@gitlab.com:rndnet/apps/common.git
git clone git@gitlab.com:rndnet/apps/server.git
git clone git@gitlab.com:rndnet/apps/client.git
git clone git@gitlab.com:rndnet/apps/scheduler.git
git clone git@gitlab.com:rndnet/apps/bfj.py.git
git clone git@gitlab.com:rndnet/apps/bfj.m.git
git clone git@gitlab.com:rndnet/apps/bfj.r.git
#git clone git@gitlab.com:rndnet/apps/bfj.jl.git


