RNAME=rabbitmq-install.sh

read -e -p "Enter downloading directory for "${RNAME}": " -i  $(pwd) INSTALL_DIR

wget https://gitlab.com/rndnet/devops/update-scripts/local/-/raw/master/${RNAME}?inline=false -O ${INSTALL_DIR}/${RNAME}

chmod -v +x ${INSTALL_DIR}/${RNAME}

