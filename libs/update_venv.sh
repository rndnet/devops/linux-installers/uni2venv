ABSOLUTE_FILENAME=`readlink -e "$0"`
FILE_DIRECTORY=`dirname "$ABSOLUTE_FILENAME"` # Define script directory
if [ ! -f "$FILE_DIRECTORY/common.sh" ];  then
    echo "File "$(pwd)"/common.sh not found!" ;  exit 1 ;
fi
. $FILE_DIRECTORY/common.sh

clear

eval ${conda_init_script}
conda env list

echo 
read -e -p "Enter venv path (exist or new): " venv_personal
#venv_personal=${venv_src}/${venv_personal}

echo "Choose RnDnet bundle:"
echo "------------------------------------------"
echo "1) Compute: "   ${compute_node_bundle[@]}
echo "2) Client: "    ${client_bundle[@]}
echo "3) Server: "    ${smaster_bundle[@]}
echo "4) Developer: " ${develop_bundle[@]}
echo "5) Full: "      ${super_bundle[@]}
echo "------------------------------------------"
echo "0) Exit"

read doing 

case $doing in
    1)
       personal_bundle=( "${compute_node_bundle[@]}")
       ;;
    2)
       personal_bundle=( "${client_bundle[@]}")
       ;;
    3)
       personal_bundle=( "${smaster_bundle[@]}")
       ;;
    4)
       personal_bundle=( "${develop_bundle[@]}")
       ;;
    5)
       personal_bundle=( "${super_bundle[@]}")
       ;;
    0)
       exit
       ;;
    *)
      echo Unknown bundle
      exit  
      ;;
esac 

###-------------------------------------------

echo Install modules: ${personal_bundle[@]}
install_array personal_bundle[@]  ${venv_personal}

echo "Current python" $(which python)
