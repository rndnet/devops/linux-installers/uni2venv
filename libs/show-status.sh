#!/bin/bash

# Copy to directory whith common_params.sh script!!! 

clear
. common_params.sh

minios=( minio )           # Mminio servers services list
dbs=( cloud_db )           # Project bases for monitoring
login_node="ssh login0"    # Compute nodes login node if exist. Make empty if login node not exist 
db_host=localhost          # db host

while [ 1=1 ] 
do

ind=0

smaster_services=("${smaster_servers_services[@]}" "${smaster_schedulers_services[@]}")
for i in "${smaster_services[@]}"
do
    echo ${ind}") "${i}
    let "ind++"
done
for i in "${minios[@]}"
do
    echo ${ind}") "${i}
    let "ind++"
done

echo "i) Show instances"
echo "q) Show queues"
echo "e) Exit"

echo
read -p "Input service index or command : " SERVICE_INDEX

if [ $SERVICE_INDEX = e  ]; then
    exit 0
elif [ $SERVICE_INDEX = q  ]; then
    $login_node squeue
    echo
elif [ $SERVICE_INDEX = i  ]; then

    for i in "${dbs[@]}"
    do
        echo
        echo ------------------------------------
        psql -U bf_admin -h $db_host -d ${i} -c 'select id, node, job_id, ret_code, created, archived,  scheduler_host, episode, runtime, path from blockflow.instance order by id desc limit 10;'
        echo ------------------------------------
        psql -U bf_admin -h $db_host -d ${i} -c 'select id, node, job_id, ret_code, created, archived,  scheduler_host, episode, runtime, path from blockflow.instance  where archived=false order by id desc limit 10;'
        echo ------------------------------------
        echo
    done

elif [ $SERVICE_INDEX -ge ${#smaster_services[@]}  ]; then
    let "SERVICE_INDEX=${SERVICE_INDEX}-${#smaster_services[@]}"
    systemctl status ${minios[${SERVICE_INDEX}]}
    echo
else
    systemctl status ${smaster_services[$SERVICE_INDEX]}
    sudo journalctl -u ${smaster_services[$SERVICE_INDEX]} -n 100
    echo
fi

done

