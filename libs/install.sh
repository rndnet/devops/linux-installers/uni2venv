clear
#set -vx

CUR_DIR=$(pwd)
echo "**************************"
echo "Create "$HOME"/rndnet"
echo "**************************"
mkdir -p $HOME/rndnet
cd  $HOME/rndnet

echo
echo "**************************"
echo "Download Miniconda3-x86_64 and install"
echo "**************************"
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
echo "You must update your environment by '. ~/.bashrc' command for conda command enable"

echo
echo "**************************"
echo "Install other dependences"
echo "**************************"
echo "sudo yum install graphviz"
echo "**************************"
sudo yum install graphviz
echo "**************************"
echo "sudo yum install gcc-c++"  
echo "**************************"
sudo yum install gcc-c++  

echo "**************************"
echo "Install python 3 to base miniconda environment"  
echo "**************************"
sudo yum install python3 
. ~/miniconda3/etc/profile.d/conda.sh
conda install python=3.6

echo
echo "**************************"
echo "Copy parameters file"  
echo "**************************"
cd ${CUR_DIR}
cd ..
cp -v libs/common_params_skel.sh common_params.sh

echo
echo "**************************"
echo "Download rndnet packages or clone sources"  
echo "**************************"
./run-update-src.sh

echo
echo "**************************"
echo "Install rndnet to conda environment"  
echo "**************************"
./run-update-clients.sh
./run-update-smaster-no-restart.sh
./run-update-snodes.sh
./run-update-developers.sh

echo
echo "**************************"
echo "Install minio object storage server"
echo "**************************"
bash libs/minio-manager-installer.sh
./minio_manager.sh

echo
echo "**************************"
echo "Database install and init"
echo "**************************"
bash libs/rndnet-init-db-installer.sh
./rndnet-init-db.sh

. common_params.sh

echo
echo "**************************"
echo "Cloud '"${clouds[0]}"' init"
echo "**************************"
./run-rndnet_console.sh cloud init --host=localhost --port=5432 ${clouds[0]}

echo
echo "**************************"
echo "Install server service"
echo "**************************"
echo "You must disable firewall for remote rndnet server connections"
echo "sudo systemctl stop firewalld"
echo "**************************"
cd ${venv_smaster}/bin/
./rndnet_server_manager.sh ${clouds[0]}

echo
echo "**************************"
echo "Install scheduler service"
echo "**************************"
cd ${venv_smaster}/bin/
./rndnet_scheduler_manager.sh ${clouds[0]}

echo
echo "**************************"
echo "Register conda environments"
echo "**************************"
conda config --append envs_dirs $venv_src


