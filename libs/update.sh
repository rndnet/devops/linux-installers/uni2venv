ABSOLUTE_FILENAME=`readlink -e "$0"`
FILE_DIRECTORY=`dirname "$ABSOLUTE_FILENAME"` # Define script directory
if [ ! -f "$FILE_DIRECTORY/common.sh" ];  then
    echo "File "$(pwd)"/common.sh not found!" ;  exit 1 ;
fi
. $FILE_DIRECTORY/common.sh


if [ -f "$SCHEME_FILE" ];  then
    date_old=$(stat -c %y $SCHEME_FILE)
else
   echo $SCHEME_FILE' not exist! Can not check database scheme changes!'
fi

case $1  in
     update-src)
         
         echo "Sources/packages update"
         if [ -z "$strategy" ]; then
             echo -e "\n${RED} Error! 'strategy' variable not set in 'common_params.sh'. ${NC}"
             exit
         fi

	  if [ "$strategy" = "src" ] ; then
            update_array super_bundle[@]                
	  elif [ "$strategy" = "pkg" ] ; then
            update_array super_bundle[@]                
          else
		echo 'Unsupported command for '$strategy' strategy'
		exit 1
          fi
          ;;

     clients)

         echo "Clients environments update"
         if [ -z "$venv_client" ]; then
             echo -e "\n${RED} Warning! 'venv_client' variable not set in 'common_params.sh'. Skip. ${NC}"
             exit
         fi

          for cur_client in "${venv_client[@]}"
          do
             echo -e "\n${RED} Install client  bundle to ${cur_client} ${NC}"
             install_array client_bundle[@] ${cur_client}
          done
          ;;

     snodes)
         
         echo "Compute nodes environments update"
         if [ -z "$venv_snode" ]; then
             echo -e "\n${RED} Warning! 'venv_snode' variable not set in 'common_params.sh'. Skip. ${NC}"
             exit
         fi

          for cur_snode in "${venv_snode[@]}"
          do
             echo -e "\n${RED} Install snode bundle to ${cur_snode} ${NC}"
             install_array compute_node_bundle[@] ${cur_snode}
          done
          ;;

     developers)
          
         echo "Developers environments update"
         if [ -z "$venv_developer" ]; then
             echo -e "\n${RED} Warning! 'venv_developer' variable not set in 'common_params.sh'. Skip. ${NC}"
             exit
         fi
        
          for cur_dev in "${venv_developer[@]}"
          do
             echo -e "\n${RED} Install developer bundle to ${cur_dev} ${NC}"
             install_array develop_bundle[@] ${cur_dev}
          done
          ;;

     smaster)

         echo "Smasters environments update"
         if [ -z "$venv_smaster" ]; then
             echo -e "\n${RED} Warning! 'venv_smaster' variable not set in 'common_params.sh'. Skip. ${NC}"
             exit
         fi
          
         install_array smaster_bundle[@] $venv_smaster

         ;;

    smaster-restart)
         
	 if [ -z "$smaster_update_servers_services" ]; then
            echo -e "\n${RED}Updated mirror servers services not set. Skip.${NC}\n"
         else
	    echo -e "\n${RED}Start updated mirror servers...${NC}\n"
            start_array smaster_update_servers_services[@]
	    sleep 20
            
	    if [ -z "$smaster_pre_update" ]; then
               echo -e "\n${RED}Pre-update variable not set. Skip.${NC}\n"
            else
               eval "$smaster_pre_update"
            fi
         fi
         
         echo "Services restart"
         
         if [ -z "$smaster_servers_services" ]; then
            echo -e "\n${RED}Servers services not set. Skip.${NC}\n"
         else
	    echo -e "\n${RED}Stopping servers...${NC}\n"
            stop_array smaster_servers_services[@]
         fi
         
         if [ -z "$smaster_schedulers_services" ]; then
            echo -e "\n${RED}Schedulers services not set. Skip.${NC}\n"
         else
	    echo -e "\n${RED}Stopping schedulers...${NC}\n"
            stop_array smaster_schedulers_services[@]
         fi
         
         if [ -z "$smaster_servers_services" ]; then
            echo -e "\n${RED}Servers services not set. Skip.${NC}\n"
         else
	    echo -e "\n${RED}Starting servers...${NC}\n"
            start_array smaster_servers_services[@]
            sleep 10
         fi

	 if [ -z "$smaster_update_servers_services" ]; then
            echo -e "\n${RED}Updated mirror servers services not set. Skip.${NC}\n"
         else
	    echo -e "\n${RED}Stop updated mirror servers...${NC}\n"
	    
	    if [ -z "$smaster_post_update" ]; then
               echo -e "\n${RED}Post-update variable not set. Skip.${NC}\n"
            else
              eval "$smaster_post_update"
            fi

            stop_array smaster_update_servers_services[@]
         fi
         
         if [ -z "$smaster_schedulers_services" ]; then
            echo -e "\n${RED}Schedulers services not set. Skip.${NC}\n"
         else
	    echo -e "\n${RED}Starting schedulers...${NC}\n"
            start_array smaster_schedulers_services[@]
         fi

          ;;
     *)
          echo -e  "${RED}Unknown command: $1 ${NC}"
          echo Use commands: clients, developers, snodes, smaster, update-src, smaster-restart
          exit
          ;;
esac

if [ -f "$SCHEME_FILE" ];  then
    date_new=$(stat -c %y $SCHEME_FILE)
    if [ "$date_old" != "$date_new" ] ; then
        echo -e "\n ${SCHEME_FILE} ${RED}has changed! You must run database update script!${NC}"
    fi
else
    echo $SCHEME_FILE' not exist! Can not check database scheme changes!'
fi

