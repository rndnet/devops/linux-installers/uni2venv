ABSOLUTE_FILENAME=`readlink -e "$0"`
FILE_DIRECTORY=`dirname "$ABSOLUTE_FILENAME"` # Define script directory
if [ ! -f "$FILE_DIRECTORY/../common_params.sh" ];  then
   echo "$FILE_DIRECTORY/../common_params.sh not found!" ;  exit 1 ; 
fi
. $FILE_DIRECTORY/../common_params.sh

eval ${conda_init_script}

RETC=0

echo "Smaster conda env"
conda activate $venv_smaster
if [ $? = 0 ]; then

  if [ -z "$clouds" ]; then
       echo "The 'clouds' variable not set in 'common_params.sh'. Skip."
  else

    for i in "${clouds[@]}"
    do
        rndnet_console cloud patch -l ${i}
        if [[ $? == 1 ]]; then
          echo "Patching error!"
          RETC=1
        fi

      echo
    done
  fi  

  conda deactivate
fi

if [ "${conda_init_script}" != "" ]; then
    conda deactivate # deactivate conda base
fi

echo "Current python: '"$(which python)"'"

exit $RETC
