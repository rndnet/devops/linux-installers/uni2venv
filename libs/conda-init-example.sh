#!/bin/bash
# Use source command to activate this

# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/nfs0/r003/rndnet/soft/miniconda3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
   eval "$__conda_setup"
else
   if [ -f "/nfs0/r003/rndnet/soft/miniconda3/etc/profile.d/conda.sh" ]; then
      . "/nfs0/r003/rndnet/soft/miniconda3/etc/profile.d/conda.sh"
   else
      export PATH="/nfs0/r003/rndnet/soft/miniconda3/bin:$PATH"
   fi
fi
unset __conda_setup
# <<< conda initialize <<<


