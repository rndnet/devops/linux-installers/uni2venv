#!/bin/bash
. common_params.sh
if [ ! -f "common_params.sh" ];  then
        echo "File common_params.sh not found!" ;  exit 1 ;
fi

clear

eval ${conda_init_script}
conda env list

echo
read -e -p  "Enter conda env name or path: " env_cur

conda activate ${env_cur}

if [ $? = 0 ]; then

    cycle=1
    while [ $cycle = 1 ]
    do
        read -e -p  "Enter command or exit: " cmd_cur

        if [ cmd_cur = "exit" ]; then
            cycle=0
        else
          echo "Current python: '"$(which python)"'"
          echo
          eval ${cmd_cur}
        fi
        echo
    done

    conda deactivate
fi


if [ "${conda_init_script}" != "" ]; then
   conda deactivate # deactivate conda base
fi

echo "Current python: '"$(which python)"'"




